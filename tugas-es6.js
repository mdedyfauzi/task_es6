// no. 1
const identityCard = ['Boy', 'Dec 31, 1991', 'Boulevard', 'O', 'Male'];

// no. 2
const house = {
  number: 1,
  color: 'blue',
  material: 'wood',
  size: {
    length: 1000,
    width: 2000,
  },
};

// no. 3
const [name, birthDate, aderess, bloodType, gender] = identityCard;
const { number, color, material, size } = house;
console.log(name, birthDate, aderess, bloodType, gender, number, color, material, size);

// no. 4
console.log(`House no. ${number} at ${aderess} street was ${name} propertys`);

// no. 5
const lengthWide = size[0] > size[1] ? true : false;
console.log(lengthWide);

// no. 6
const clubs = [
  { club: 'ManUnited', win: 0 },
  { club: 'ManCity', win: 0 },
  { club: 'Chelsea', win: 0 },
  { club: 'Arsenal', win: 1 },
  { club: 'Liverpool', win: 0 },
];

// no. 7
let invicible = clubs.map((data) => data);
console.log(invicible);

// no. 8
let mostInvicible = clubs.sort((a, b) => b.win - a.win);
console.log(mostInvicible);

// no. 9
let onlyInvicible = clubs.find((a) => a.win > 0);
console.log(onlyInvicible);
