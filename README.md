# TASK ES6
week 3 day 1 - task understanding ES6 feature
**Deadline**: Wednesday, 20 October 2021, 21:00 WIB

## Instructions

- Try to complete complete the task
- Make your own git repository
- Push your result in your git repository

## Task Requirements

- [x] Buat variable yang isinya array. Namanya bebas kalo bisa sesuaikan konteks. Di dalam array minimal ada 5 data.
- [x] Buat variable yang isinya object. Nama variablenya bebas kalo bisa sesuaikan konteks. Di dalam object minimal ada 4 property. 1 property nilainya harus object lagi <- dalam object tersebut bebas mau di kasi property apa, tapi kalo bisa sesuaikan konteks
- [x] Desctrucing ke 2 variable tersebut. Terus hasil desctructnya tampilin di console ya men-temen
- [x] Buat satu console.log yang nemampilkan data string sama expressi ( variable aja ) pakek template literals ya
- [x] Buat satu variable juga yang dalamnya ada pengkondisian pakek ternary operator. Kemudian hasilnya boleh tampilin lewat console.log
- [x] Buat variable yang isinya array of object. minimal ada 5 data object
- [x] Terus variable array yang no 6. boleh di looping pakek method map dan hasil loopinganya tampilin di console ya guys.
- [x] Masi berhubungan sama variable yang di no 6, silakan buat variable yang isinya hasil dari eksekusi method filter ya guys
- [x] Masi berhubungan sama variable yang di no 6 juga, silakan buat variable yang isinya hasil dari eksekusi method find ya guys.

## References

- ##########
